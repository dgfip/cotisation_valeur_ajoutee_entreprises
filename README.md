# COTISATION SUR LA VALEUR AJOUTÉE DES ENTREPRISES (CVAE)

La cotisation sur la valeur ajoutée des entreprises (CVAE) constitue le
deuxième élément de la contribution économique territoriale (CET). Le
produit de cet impôt est réparti entre les collectivités territoriales
(communes, établissements publics de coopération inter-communale,
départements) sur le territoire desquelles l'entreprise est localisée.

Sont légalement assujetties à la CVAE, les personnes physiques ou
morales qui exercent une activité imposable à la cotisation foncière des
entreprises (CFE) et dont le chiffre d'affaires (CA) est supérieur à
152 500€ hors taxes. Cependant, les entreprises dont le chiffre
d'affaires est strictement inférieur à 500 000€ hors taxes bénéficient,
lorsqu'elles ne remplissent pas les conditions pour être membres d'un
groupe «économique»[^1], d'un dégrèvement total de cette cotisation.

La CVAE est un impôt auto-liquidé dû par le redevable qui exerce
l'activité au 1er janvier de l'année d'imposition. Pour la détermination
de la CVAE, sont retenus le chiffre d'affaires réalisé et la valeur
ajoutée produite au cours d'une période de référence.

Deux acomptes sont à verser au plus tard le 15 juin et le 15 septembre
et le redevable doit procéder à la liquidation définitive de la CVAE au
plus tard le deuxième jour ouvré du mois de mai de l'année suivant celle
de l'imposition.

Par ailleurs, tout assujetti à la CVAE (chiffre d'affaires supérieur à
152 500€) a l'obligation de souscrire une déclaration mentionnant la
valeur ajoutée produite au cours de la période de référence ainsi que la
répartition des effectifs salariés par établissement ou lieu d'emploi.

**Le traitement dit «de répartition»** consiste à déterminer les
montants de CVAE qui seront reversés aux collectivités locales
(Département, Commune et EPCI à fiscalité propre).

Les produits de CVAE à répartir entre les collectivités locales en N+1
sont constitués des 2 acomptes versés au plus tard les 15 juin et 15
septembre N et du solde de N-1 versé au plus tard le 2e jour ouvré du
mois de mai N. Ces produits sont majorés des montants encaissés au cours
de cette même année N après l'expiration des délais légaux de paiement,
le cas échéant, du dégrèvement barémique et du dégrèvement transitoire
pour pertes[^2] et des impositions supplémentaires recouvrées la même
année N. Ces produits sont minorés des restitutions accordées au cours
de la même année (notamment les restitutions d'excédents versées dans
les 60 jours du dépôt du relevé de solde).

La loi n° 2020-1721 du 29 décembre 2020 de finances pour 2021 prévoit, à
compter du millésime d'imposition 2021, un abaissement du taux
d'imposition à la CVAE à hauteur de la part affectée à l'échelon
régional, soit 50% du produit de la CVAE. Corrélativement, la part de
CVAE revenant aux régions est compensée par une fraction de la TVA dès
la répartition 2021.

À compter de la répartition effectuée en 2021, seuls les départements et
le bloc communal perçoivent des montants de CVAE selon les taux de
répartition suivants

![](images/Taux_de_r%C3%A9partition_CVAE.jpg)

**Deux clefs de répartition existent :**

- Une clef de répartition principale : la valeur ajoutée est répartie
  pour un 1/3 au prorata des valeurs locatives foncières (VLF) des
  immobilisations imposées à la CFE et pour 2/3 au prorata des effectifs
  salariés déclarés sur la déclaration 1330-CVAE-SD. Lorsque la
  déclaration n°1330-CVAE-SD de l'année en cours fait défaut, la fraction
  de valeur ajoutée se rapportant aux effectifs est répartie sur la base
  de la déclaration 1330-CVAE-SD de l'année précédente ;

- Une clef de répartition de substitution : si les déclarations
  n°1330-CVAE-SD de l'année en cours et de l'année précédente font défaut,
  la valeur ajoutée est répartie au prorata des seules Valeurs Locatives
  Foncières (VLF) des locaux imposables à la CFE.

Les VLF et les effectifs des établissements industriels étaient pondérés
respectivement par des coefficients de 21 et 5. Pour les établissements
industriels dont la base d'imposition à la cotisation foncière des
entreprises (CFE) ou à la taxe foncière est calculée selon la méthode
dite «comptable», fondée sur la valeur des immobilisations inscrites au
bilan, la loi de finances pour 2021 révise le calcul en réduisant les
taux d'intérêt applicables au prix de revient des différents éléments
des établissements industriels, ce qui permet de diviser par deux leur
valeur d'assiette. Néanmoins, afin de maintenir le poids relatif des
établissements industriels dans la clé de répartition de la CVAE entre
les collectivités territoriales, le coefficient de pondération des
valeurs locatives de ces établissements est porté de 21 à 42. Le
coefficient de pondération des effectifs n'est pas modifié. Cette
pondération est appliquée pour les estimations des montants à reverser
en 2022.

**Schématiquement le traitement de répartition** est le suivant :

![](images/Synoptique_des_traitements_CVAE.png)

**Deux traitements dit de «pré-simulation» et «simulation»**
respectivement en juillet/août N et en octobre/novembre N estiment les
montants qui seront versés en N+1 aux collectivités locales permettant
ainsi d'obtenir de la visibilité pour établir leur budget. En
janvier/février N+1, un traitement dit de «répartition» détermine
définitivement les montants qui seront versés aux collectivités en N+1
(1/12e).

Une campagne complète de répartition N se décompose en trois volets :

-   une première répartition estimative dite « **pré-simulation** » à
    l'été de l'année N permet d'établir une première tendance des
    montants CVAE à reverser aux collectivités  en N+1 et qui se base
    sur les données comptables disponibles du 1^er^ janvier au 30 juin N
    ;
-   une seconde répartition estimative dite **« simulation »** à
    l'automne de l'année N permet de fournir des chiffres prévisionnels
    affinés et qui se base sur les données comptables disponibles du 1er
    janvier au 30 septembre N ;
-   une **répartition définitive** fige les montants au titre de N
    (réalisée en début N+1) à reverser aux collectivités territoriales
    qui mobilise l'ensemble des données comptables relatives à
    l'année N.

**La cinématique des traitements de répartition dans le temps (N
désignant le millésime de reversement) **est la suivante :

![](images/Calendrier_des_traitements_CVAE.png)

**<u>Nature des produits de CVAE à reverser aux collectivités</u>**

Au titre d'une entreprise donnée, la CVAE à reverser en N aux
collectivités territoriales bénéficiaires est constituée des éléments
suivants :

**la CVAE payée** : elle est constituée, selon une logique de caisse, de
l'ensemble des paiements effectifs réalisés au cours de l'année N-1
(quelle qu'en soit la nature : acomptes, solde, paiements tardifs,
paiements suite à rehaussement) diminués des remboursements (hors
contentieux déposés) effectués au cours de la même période.

**la CVAE dégrevée** : elle correspond au montant de dégrèvement automatique
dit «barémique» pris en charge par l'État. Il représente la part de CVAE
non versée par l'entreprise en raison de l'utilisation d'un taux
effectif d'imposition déterminé selon un barème progressif variable
suivant le chiffre d'affaires réalisé, au lieu du taux théorique de
0,75%[^3].

**la CVAE exonérée compensée** : elle résulte de l'application d'une
exonération sur la valeur ajoutée en faveur d'un établissement de
l'entreprise. L'application d'une exonération est subordonnée à la
demande formulée par l'entreprise pour un établissement donné et de la
délibération, ou de l'absence de délibération des collectivités
concernées en fonction de la nature de l'exonération

La détermination des montants CVAE afférents à l'ensemble des
entreprises (assujetties et assujetties redevables) à reverser en N+1
aux collectivités territoriales est réalisée par un traitement national
(programmes MTPCR45 et MTPCR50) qui s'appuie sur les éléments suivants :

-   <u>le fichier national des déclarations n°1330-CVAE-SD ou assimilés</u>
    (par mesure de simplification les entreprises peuvent renseigner
    les données afférentes à la CVAE sur les formulaires de la série E
    des liasses fiscales) relatives à l'exercice N-1 : il permet de
    disposer, outre des éléments d'assiette de chiffre d'affaires et de
    valeur ajoutée, des effectifs salariés et de leur localisation
    géographique, deuxième composante de la clé de répartition ;
-   le fichier national des déclarations n°1329 (définitive et acomptes) déposées en N : il permet de disposer de l’ensemble des opérations comptables (encaissements/décaissements) réalisés en N ;
-   ​	le fichier national des déclarations n°1329 (définitive et acomptes) déposées en N : il permet de disposer de l’ensemble des opérations comptables (encaissements/décaissements) réalisés en N ;
-   ​	le fichier national des déclarations n°1329 (définitive et acomptes) déposées en N : il permet de disposer de l’ensemble des opérations comptables (encaissements/décaissements) réalisés en N ;
-   ​	le fichier national des déclarations n°1329 (définitive et acomptes) déposées en N : il permet de disposer de l’ensemble des opérations comptables (encaissements/décaissements) réalisés en N ;
-   <u>le fichier national des rôles généraux CFE N</u> : il permet de
    disposer des valeurs locatives foncières des établissements d'une
    entreprise, ainsi que des exonérations applicables à chaque
    établissement ;
-   <u>le fichier national FDL N</u> : il permet de connaître le périmètre
    des collectivités territoriales, ainsi que les délibérations des
    collectivités en matière d'exonération.

[^1]: Les entreprises membres d'un groupe « économique » sont celles
    remplissant les conditions pour devenir membre d'un groupe fiscal
    pour l'assiette et le paiement de l'impôt sur les sociétés, que
    l'option pour ce régime d'imposition ait ou non été exercée. Dans ce
    cas, les entreprises concernées sont imposées à la CVAE en fonction
    d'un taux prenant en compte le chiffre d'affaires consolidé du
    groupe.

[^2]: Pour sa partie imputée sur la CVAE due. Pour mémoire, le
    dégrèvement transitoire n'est plus applicable à compter des
    impositions CVAE dues au titre des années 2014 et suivantes.

[^3]: Taux applicable à compter du millésime d'imposition 2021. Ce taux
    se situait précédemment à 1,5 %.


Nota : La loi n° 2020-1721 du 29 décembre 2020 de finances pour 2021 prévoit, à compter du millésime d’imposition 2021, un abaissement du taux d’imposition à la CVAE à hauteur de la part affectée à l’échelon régional, soit 50% du produit de la CVAE. Corrélativement, la part de CVAE revenant aux régions est compensée par une fraction de la TVA dès la répartition des montants entre les collectivités locales en 2021.

**************

La déclaration de liquidation opérée par l’entreprise fait l’objet d’une « reliquidation » par la DGFiP afin de déterminer le montant de CVAE réellement dû par l’entreprise. Ce processus se décompose en 2 phases :

- la reliquidation proprement dite, traitement national permettant d’agréger des données nationales et de déterminer le montant réellement dû par l’entreprise ;

- la phase dite de « comparaison » entre le « réellement dû » et le « réellement payé » par l’entreprise afin de déterminer la situation de l’entreprise (excédentaire, reliquataire, ou soldée). 

  ![img](images/cinematique_generale.png)

**I. La reliquidation de la CVAE : le calcul du réellement dû** 

L’objectif du traitement de reliquidation est de procéder au recalcul des montants réellement dus de CVAE, de taxe additionnelle à la CVAE et de frais. En effet, lors de la liquidation du solde de CVAE, une entreprise n’est pas toujours en mesure de calculer précisément les impacts d’une exonération sur le montant total à verser. Chaque type d’exonération de valeur ajoutée s’applique de façon différenciée à chacun des établissements de l’entreprise en fonction des délibérations ou absences de délibérations de chaque niveau de collectivité (commune, EPCI, département). Seule l’administration dispose, en vision centrale uniquement, de l’ensemble des informations, c’est à dire toutes les demandes d’exonération par établissement et par typologie ainsi que toutes les délibérations des collectivités locales. De ce fait, ce traitement est opéré, de manière centralisée, au niveau national trois fois par an. 

Les résultats de ce traitement de reliquidation sont ultérieurement intégrés dans l’application comptable, afin de comparer le résultat de la reliquidation avec les montants réellement payés.

Le traitement utilise les fichiers suivants :

- MECV : données d’assiette (déclaratif issu des formulaires n°1329) ;
- FDL : délibérations des collectivités locales ;
- CFE : exonérations demandées par les entreprises.

**II. Les conséquences de la reliquidation : calcul du réellement payé** 

Les impôts professionnels sont stockés, dans l’application comptable, dans deux tables distinctes : La première qui correspond aux données déclarées et/ou reliquidées et la seconde aux données comptables (paiements).

Suite à l’intégration dans l’application comptable des données issues de la reliquidation, le programme compare le montant « réellement dû » calculé, à partir de la table des éléments déclarés et reliquidés, au « réellement payé » (unique ou fractionné, télépayé ou par chèque) (table des éléments comptables). 

Il en ressort  : 

1) une comparaison du « réellement dû » et du « réellement payé » (MDGCM10) 

Le traitement de comparaison confronte les données issues du traitement de reliquidation avec les données de paiement et de remboursement présentes dans Médoc. Il s’agit de comparer le «réellement dû» par l’entreprise avec le «réellement payé». Ce traitement a donc pour objet de déterminer pour chaque redevable de la CVAE dont la reliquidation a été possible, au titre d’un millésime:

- si un solde reste dû ;
- si un excédent de versement doit être restitué après contrôle de présence de restes à recouvrer par exemple ;
- si la situation peut être considérée comme soldée.  

Les redevables qui sont en insuffisance de paiement d’acomptes et/ou de solde sont considérés comme reliquataires et doivent régulariser leur situation. 

Toute situation excédentaire suppose un remboursement à l’entreprise sauf situation particulière (présence de reste à recouvrer par exemple). 

2) une validation de la comparaison, inscription des données nécessaires au paiement ou à la pénalisation (table dédiée au résultat de la reliquidation/comparaison) et comptabilisation de l’excédent (MDGCM20) 

Une situation excédentaire suppose que le module de comparaison aboutit à un montant «réellement dû» inférieur au montant «réellement payé ». Le traitement de comptabilisation automatique de l’excédent est un traitement interne à celui de la comparaison et lors du traitement de comparaison, les excédents dégagés sont traités par batch en comptabilisant des écritures automatiques d’affectation en affaire de l’excédent constaté. 

3) un contrôle de l’environnement du virement dont les coordonnées bancaires (MDGCM80) 

Le processus vérifie la présence d’une coordonnée bancaire valide fournie par l’entreprise redevable ainsi que la présence d’une opposition. Un rejet de remboursement automatique est alors effectué. Le remboursement peut, suite à l’analyse du service, être réalisé de façon manuelle.

4) la génération des prises en charges pour les millésimes reliquataires

Les prises en charge sont différentes selon le résultat de la comparaison (reliquataire, soldé ou excédentaire). En effet, dans le cas d’un reliquat, les pénalités s’appliqueront en même temps que la saisie des droits à rappeler, alors que pour un dossier soldé ou excédentaire, seules les pénalités seront prises en charge. Pour mémoire, sont pénalisés les insuffisances de versement, le défaut de télérèglement, le retard de paiement.

[^1]: Les entreprises membres d'un groupe « économique » sont celles
    remplissant les conditions pour devenir membre d'un groupe fiscal
    pour l'assiette et le paiement de l'impôt sur les sociétés, que
    l'option pour ce régime d'imposition ait ou non été exercée. Dans ce
    cas, les entreprises concernées sont imposées à la CVAE en fonction
    d'un taux prenant en compte le chiffre d'affaires consolidé du
    groupe.

[^2]: Pour sa partie imputée sur la CVAE due. Pour mémoire, le
    dégrèvement transitoire n'est plus applicable à compter des
    impositions CVAE dues au titre des années 2014 et suivantes.

[^3]: Taux applicable à compter du millésime d'imposition 2021. Ce taux
    se situait précédemment à 1,5 %.
